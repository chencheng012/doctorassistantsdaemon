#include <QCoreApplication>
#include <QProcess>
#include <QTimer>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString process_name("DoctorAssistants.exe");
    auto process = new QProcess;
    process->setProcessChannelMode(QProcess::MergedChannels);
    auto timer = new QTimer;
    QObject::connect(timer,&QTimer::timeout,[=](){
        process->start("tasklist.exe");
        if(process->waitForFinished()){
            QByteArray result = process->readAll();
            bool is_running = result.contains(process_name.toLocal8Bit());
            if(!is_running){
                qDebug()<<process_name << "not running it will be start !!";
                QProcess start_process;
                if(start_process.startDetached(process_name)){
                    qDebug()<<process_name << "start success !!";
                }else{
                    qDebug()<<process_name << "start failed !!";
                }
            }
        }
    });
    timer->start(5000);

    return a.exec();
}
